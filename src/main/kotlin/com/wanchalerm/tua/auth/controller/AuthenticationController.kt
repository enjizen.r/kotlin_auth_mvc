package com.wanchalerm.tua.auth.controller

import com.wanchalerm.tua.auth.model.request.AuthenticationRequest
import com.wanchalerm.tua.auth.model.request.AuthorizationRequest
import com.wanchalerm.tua.auth.model.response.AuthenticationResponse
import com.wanchalerm.tua.auth.model.response.CustomerResponse
import com.wanchalerm.tua.auth.service.authentication.AuthenticationService
import com.wanchalerm.tua.auth.service.authorized.AuthorizedService
import com.wanchalerm.tua.common.constant.ResponseEnum
import com.wanchalerm.tua.common.model.response.ResponseModel
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1")
class AuthenticationController(private val authenticationService: AuthenticationService,
                               private val authorizedService: AuthorizedService) {

    @PostMapping("/verify/pass")
    fun verifyPassword(@RequestBody authenticationRequest: AuthenticationRequest): ResponseEntity<ResponseModel<AuthenticationResponse>> {
        return ResponseEntity.ok(ResponseModel<AuthenticationResponse>(responseEnum = ResponseEnum.SUCCESS).setDataObj(authenticationService.verifyPassword(authenticationRequest)))
    }

    @PostMapping("/verify/token")
    fun verifyToken(@RequestBody request: AuthorizationRequest): ResponseEntity<ResponseModel<Any>> {
        authorizedService.verifyToken(request.token!!)
        return ResponseEntity.ok(ResponseModel(responseEnum = ResponseEnum.SUCCESS))
    }
}