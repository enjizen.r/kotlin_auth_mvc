package com.wanchalerm.tua.auth.model.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.LocalDateTime

@Entity
@Table(name = "authentication_token")
class AuthenticationTokenEntity(
    @field:Id
    @field:Column(name = "token", length = 255, nullable = false)
    val token: String,

    @field:Column(name = "customer_code", length = 36, nullable = false)
    val customerCode: String,

    @field:Column(name = "expiration_time", nullable = false)
    val expirationTime: LocalDateTime
)