package com.wanchalerm.tua.auth.service.authorized

interface AuthorizedService {
    fun verifyToken(token: String)
}