package com.wanchalerm.tua.auth.service.authentication

import com.wanchalerm.tua.auth.model.request.AuthenticationRequest
import com.wanchalerm.tua.auth.model.response.AuthenticationResponse

interface AuthenticationService {
    fun verifyPassword(authenticationRequest: AuthenticationRequest): AuthenticationResponse?
}