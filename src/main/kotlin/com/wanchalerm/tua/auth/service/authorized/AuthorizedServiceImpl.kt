package com.wanchalerm.tua.auth.service.authorized

import com.wanchalerm.tua.auth.client.CustomerClient
import com.wanchalerm.tua.auth.repository.AuthenticationTokenRepository
import com.wanchalerm.tua.auth.util.jwt.JwtService
import com.wanchalerm.tua.common.exception.UnauthorizedException
import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class AuthorizedServiceImpl(private val customerClient: CustomerClient,
                            private val authenticationTokenRepository: AuthenticationTokenRepository,
                            private val jwtService: JwtService
) : AuthorizedService {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)
    override fun verifyToken(token: String) {
        val claims = extractClaimsOrThrow(token)
        logTokenVerification(claims)
        validateTokenInRepository(token)
    }

    private fun extractClaimsOrThrow(token: String): Claims {
        return try {
            jwtService.extractAllClaims(token)
        } catch (ex: ExpiredJwtException) {
            throw UnauthorizedException(message = "Token is expired", throwable = ex)
        }
    }

    private fun logTokenVerification(claims: Claims) {
        logger.info(
            "Verifying token with customer_code: {}",
            claims["customer_code"]
        )
    }

    private fun validateTokenInRepository(token: String) {
        if (!authenticationTokenRepository.existsByToken(token)) {
            throw UnauthorizedException(message = "Invalid token")
        }
    }
}