package com.wanchalerm.tua.auth.service.authentication

import com.wanchalerm.tua.auth.client.CustomerClient
import com.wanchalerm.tua.auth.model.entity.AuthenticationTokenEntity
import com.wanchalerm.tua.auth.model.request.AuthenticationRequest
import com.wanchalerm.tua.auth.model.response.AuthenticationResponse
import com.wanchalerm.tua.auth.model.response.CustomerResponse
import com.wanchalerm.tua.auth.repository.AuthenticationTokenRepository
import com.wanchalerm.tua.auth.util.jwt.JwtService
import com.wanchalerm.tua.common.constant.ResponseEnum
import com.wanchalerm.tua.common.exception.BusinessException
import com.wanchalerm.tua.common.extension.toLocalDateTime
import org.springframework.stereotype.Service

@Service
class AuthenticationServiceImpl(private val customerClient: CustomerClient,
                                private val authenticationTokenRepository: AuthenticationTokenRepository,
                                private val jwtService: JwtService): AuthenticationService {

    override fun verifyPassword(authenticationRequest: AuthenticationRequest): AuthenticationResponse? {
        val customerResponse = requireCustomerResponse(authenticationRequest)
        val token = createAndSaveJwtToken(authenticationResponse = customerResponse, username = authenticationRequest.username!!)

        return AuthenticationResponse(token = token, customerCode = customerResponse.customerCode)
    }

    private fun requireCustomerResponse(authenticationRequest: AuthenticationRequest): CustomerResponse {
        val response = customerClient.verifyPassword(authenticationRequest)
        if (response.status?.code != ResponseEnum.SUCCESS.code) {
            throw BusinessException(code = response.status?.code.orEmpty(), message = response.status?.message.orEmpty())
        }

        return response.dataObj as CustomerResponse
    }

    private fun createAndSaveJwtToken(authenticationResponse: CustomerResponse, username: String): String {
        val (token, expiration) = jwtService.createToken(mapOf("customer_code" to authenticationResponse.customerCode), username)

        val authenticationTokenEntity = AuthenticationTokenEntity(
            token = token,
            customerCode = authenticationResponse.customerCode,
            expirationTime = expiration.toLocalDateTime()
        )
        authenticationTokenRepository.save(authenticationTokenEntity)
        return token
    }

}