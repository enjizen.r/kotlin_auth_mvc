package com.wanchalerm.tua.auth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication(
	scanBasePackages = [
		"com.wanchalerm.tua.auth.controller",
		"com.wanchalerm.tua.auth.service",
		"com.wanchalerm.tua.auth.repository",
		"com.wanchalerm.tua.auth.aspect",
		"com.wanchalerm.tua.auth.util",
		"com.wanchalerm.tua.common.config",
		"com.wanchalerm.tua.common.exception",
		"com.wanchalerm.tua.common.filter"]
)
@EnableDiscoveryClient
@EnableFeignClients(basePackages = ["com.wanchalerm.tua.auth.client"])
class KotlinAuthMvcApplication

fun main(args: Array<String>) {
	runApplication<KotlinAuthMvcApplication>(*args)
}
