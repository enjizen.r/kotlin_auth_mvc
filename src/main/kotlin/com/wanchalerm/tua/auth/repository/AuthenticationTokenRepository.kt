package com.wanchalerm.tua.auth.repository

import com.wanchalerm.tua.auth.model.entity.AuthenticationTokenEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AuthenticationTokenRepository: JpaRepository<AuthenticationTokenEntity, String> {
    fun existsByToken(token: String): Boolean
}