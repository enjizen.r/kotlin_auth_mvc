package com.wanchalerm.tua.auth.client

import com.wanchalerm.tua.auth.model.request.AuthenticationRequest
import com.wanchalerm.tua.auth.model.response.CustomerResponse
import com.wanchalerm.tua.common.model.response.ResponseModel
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@FeignClient(value = "customer", url = "http://localhost:8011", path = "/customer")
interface CustomerClient {
    @PostMapping("/v1/verify/pass")
    fun verifyPassword(@RequestBody authenticationRequest: AuthenticationRequest): ResponseModel<CustomerResponse>
}