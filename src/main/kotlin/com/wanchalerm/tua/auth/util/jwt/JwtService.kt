package com.wanchalerm.tua.auth.util.jwt

import io.jsonwebtoken.Claims
import java.util.Date

interface JwtService {

    fun createToken(claims: Map<String, Any?>, userName: String): Pair<String, Date>

    fun extractAllClaims(token: String): Claims
}