package com.wanchalerm.tua.auth.util.jwt

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.security.Key
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date


@Component
class JwtServiceImpl: JwtService {
    @Value("\${jwt.secret}")
    private lateinit var secret: String
    override fun createToken(claims: Map<String, Any?>, userName: String): Pair<String, Date> {
        val expiration = Date.from(Instant.now().plus(30, ChronoUnit.MINUTES))
        val token = Jwts.builder()
            .setClaims(claims)
            .setSubject(userName)
            .setIssuedAt(Date(System.currentTimeMillis()))
            .setExpiration(expiration)
            .signWith(getSignKey(), SignatureAlgorithm.HS256).compact()
        return Pair(first = token, second = expiration)
    }

    override fun extractAllClaims(token: String): Claims {
        return Jwts
            .parserBuilder()
            .setSigningKey(getSignKey())
            .build()
            .parseClaimsJws(token)
            .body
    }

    private fun getSignKey(): Key {
        val keyBytes = Decoders.BASE64.decode(secret)
        return Keys.hmacShaKeyFor(keyBytes)
    }



}